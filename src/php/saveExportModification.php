<?php

// get necessary package
require_once ("server_variables.php");

// get input data
$data = $_POST['input'];
$decodeData = json_decode($data);

// create variables
$network = $decodeData->{'network'};
$imageMappedArray = $decodeData->{'imageMapped'};
$mappingData = $decodeData->{'mappingData'};
$exportInfoData = $decodeData->{'exportInfo'};
$user_dir = $decodeData->{'url'};

// create paths
$output_dir = USERFILES_DIR;
$userFile = $output_dir.$user_dir;

// get omics mapping data
$exportMapping;
foreach ($mappingData as &$mapping) {
    $target = $mapping[0];
    $type = $mapping[1];
    $name = $mapping[2];
    $style = $mapping[3];
    $scale = "";
    if ($type == "Discrete") {
        foreach ($mapping[4] as &$scaleValue) {
            $scale .= $scaleValue->{"name"}.":".$scaleValue->{"value"}.",";
        }
    }

    if ($type == "Continuous") {
        foreach ($mapping[4] as &$scaleValue) {
            $scale .= $scaleValue->{"id"}.":".$scaleValue->{"value"}.":".$scaleValue->{"styleValue"}.",";
        }
    }

    if ($type == "Identified in mapping") {
        foreach ($mapping[4] as &$scaleValue) {
            $scale .= $scaleValue->{"value"};
        }
    }
    $exportMapping .= $target."\t".$type."\t".$name."\t".$style."\t".$scale."\n";
}

// get export info
$exportInfoFile = fopen($userFile."/exportInfo.txt", "r");
$dataFile = fread($exportInfoFile, filesize($userFile."/exportInfo.txt"));
$dataFileSplit = explode(",", $dataFile);

$name = $dataFileSplit[0];
$access = $dataFileSplit[1];
$lastModification = $exportInfoData;
$bioSource = $dataFileSplit[3];
$exportUrl = $dataFileSplit[4];

$newData = $name.",".$access.",".$lastModification.",".$bioSource.",".$exportUrl;

fclose($exportInfoFile);

// remove old files
$filesToDelete = $userFile."/*.*";
shell_exec("rm -r $filesToDelete");

// files creation
$networkFile = fopen($userFile."/network.json", "w");
$listImgFile = fopen($userFile."/list_file.txt", "w");
$listMapping = fopen($userFile."/mapping.txt", "w");
$newExportInfoFile = fopen($userFile."/exportInfo.txt", "w");

// files writing
fwrite($networkFile, $network);
fwrite($listMapping, $exportMapping);
fwrite($newExportInfoFile, $newData);

// add image store
foreach ($imageMappedArray as &$value) {
    $fileName = $value[0];
    $imgEncoded = $value[1];
    file_put_contents($userFile."/".$fileName, $imgEncoded);
    fwrite($listImgFile, $fileName."\n");
}

// files closing
fclose($networkFile);
fclose($listImgFile);
fclose($listMapping);
fclose($newExportInfoFile);

echo "success";

?>
