/**
 * @author JCG
 * (a)description GirFormController : Control GIR parameters
 */

Ext.define('metExploreViz.view.form.girForm.GirFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.form-girForm-girForm',

    /**
     * Aplies event linsteners to the view
     */
    init:function(){
        let me 		= this,
            viewModel   = me.getViewModel(),
            view      	= me.getView();

        // Regex to remove bad chars in dom ids
        me.regexpPanel=/\.|>|<| |,|\/|=|\(|\)/g;

        view.on({
            fileLoad: function() {
                let name = _metExploreViz.rank.name;
                let rankData = _metExploreViz.getRankById("rankData");
                view.lookupReference('selectFile').setValue(name);
                me.parseFile(_metExploreViz.rank);

                // init params panel
                metExploreD3.fireEventArg("netEx", "switchPanel", "params");
            },
            changeOnNetwork: function() {
                if (_metExploreViz.rank.data !== undefined){
                    metExploreD3.deferFunction(function(){
                        me.parseFile(_metExploreViz.rank);
                    },100);
                }
            },
            switchPanel: function(mode) {
                if (mode === "params") {
                    view.lookupReference('netExCaption').setHidden(true);
                    view.lookupReference('gridDataDisplay').setHidden(true);
                    view.lookupReference('extractNQuitButtons').setHidden(true);

                    view.lookupReference('startNetEx').setHidden(false);
                    view.lookupReference('addNRemoveButtons').setHidden(false);
                    view.lookupReference('gridSeedSelection').setHidden(false);
                    view.lookupReference('panelLoadRankFile').setHidden(false);
                    view.lookupReference('metaboliteSearch').setHidden(false);
                }
                if (mode === "manager") {
                    view.lookupReference('netExCaption').setHidden(false);
                    view.lookupReference('gridDataDisplay').setHidden(false);
                    view.lookupReference('extractNQuitButtons').setHidden(false);

                    view.lookupReference('startNetEx').setHidden(true);
                    view.lookupReference('addNRemoveButtons').setHidden(true);
                    view.lookupReference('gridSeedSelection').setHidden(true);
                    view.lookupReference('panelLoadRankFile').setHidden(true);
                    view.lookupReference('metaboliteSearch').setHidden(true);
                }
                if (mode === "begin") {
                    view.lookupReference('netExCaption').setHidden(true);
                    view.lookupReference('gridDataDisplay').setHidden(true);
                    view.lookupReference('extractNQuitButtons').setHidden(true);

                    view.lookupReference('startNetEx').setHidden(true);
                    view.lookupReference('addNRemoveButtons').setHidden(true);
                    view.lookupReference('gridSeedSelection').setHidden(true);
                    view.lookupReference('metaboliteSearch').setHidden(true);
                }
            },
            fillStore: function(node) {
                if (node.biologicalType === "metabolite") {
                    let metaboliteStore = view.lookupReference('metaboliteDataTable').getStore();
                    let pathwayStore = view.lookupReference('pathwayDataTable').getStore();
                    let compartmentStore = view.lookupReference('compartmentDataTable').getStore();

                    metaboliteStore.add({metabolite: node.name});

                    let compartmentCheck = compartmentStore.find('compartment', node.compartment);
                    if (compartmentCheck === -1) {
                        compartmentStore.add({compartment: node.compartment});
                    }

                    if (node.pathways.length > 0) {
                        for (let i = 0; i < node.pathways.length; i++) {
                            let pathwayCheck = pathwayStore.find('pathway', node.pathways[i]);
                            if (pathwayCheck === -1) {
                                pathwayStore.add({pathway: node.pathways[i]});
                            }
                        }
                    }
                }
                if (node.biologicalType === "reaction") {
                    let reactionStore = view.lookupReference('reactionDataTable').getStore();
                    let reaction = {reaction: node.name};
                    reactionStore.add(reaction);
                }
            },
            removeStore: function(node) {
                if (node.biologicalType === "metabolite") {
                    let metaboliteStore = view.lookupReference('metaboliteDataTable').getStore();
                    let pathwayStore = view.lookupReference('pathwayDataTable').getStore();
                    let compartmentStore = view.lookupReference('compartmentDataTable').getStore();

                    metaboliteStore.removeAt(metaboliteStore.find('metabolite', node.name));

                    let session = metExploreD3.getSessionById('viz');
                    let nodesVisited = session.getD3Data().getNodes().filter(function(node) {
                        if (node.isVisited() && node.biologicalType === "metabolite"){
                            return node;
                        }
                    });

                    let pathwaysCheck = node.getPathways();
                    let pathwaysStatus = {};
                    pathwaysCheck.forEach(function(pathw){
                        pathwaysStatus[pathw] = false;
                    });

                    let compartmentCheck = node.getCompartment();
                    let compartmentStatus = false;

                    nodesVisited.forEach(function(nodeVisit){
                        if (nodeVisit.getCompartment() === compartmentCheck){
                            compartmentStatus = true;
                        }
                        nodeVisit.getPathways().forEach(function(nodeVisitPathw){
                            if (pathwaysStatus[nodeVisitPathw] === false){
                                pathwaysStatus[nodeVisitPathw] = true;
                            }
                        });
                    });

                    if (compartmentStatus === false) {
                        compartmentStore.removeAt(compartmentStore.find('compartment', compartmentCheck));
                    }

                    for (let [key, value] of Object.entries(pathwaysStatus)) {
                        if (value === false) {
                            pathwayStore.removeAt(pathwayStore.find('pathway', key));
                        }
                    }
                }
                if (node.biologicalType === "reaction") {
                    let reactionStore = view.lookupReference('reactionDataTable').getStore();
                    reactionStore.removeAt(reactionStore.find('reaction', node.name));
                }
            },
            removeAllDataStore: function() {
                let metaboliteStore = view.lookupReference('metaboliteDataTable').getStore();
                metaboliteStore.removeAll();
                let pathwayStore = view.lookupReference('pathwayDataTable').getStore();
                pathwayStore.removeAll();
                let compartmentStore = view.lookupReference('compartmentDataTable').getStore();
                compartmentStore.removeAll();
                let reactionStore = view.lookupReference('reactionDataTable').getStore();
                reactionStore.removeAll();
                let metaboliteTableGir = view.lookupReference('metaboliteTableGir').getStore();
                metaboliteTableGir.removeAll();
                let seedTableGir = view.lookupReference('seedTableGir').getStore();
                seedTableGir.removeAll();
            }
        });

        view.lookupReference('metaboliteTableGir').on({
            selectionchange: function(model, selected, eOpts) {
                if (selected.length > 0) {
                    view.lookupReference('addToSeed').enable();
                }
                else {
                    view.lookupReference('addToSeed').disable();
                }
            }
        });

        view.lookupReference('seedTableGir').on({
            selectionchange: function(model, selected, eOpts) {
                if (selected.length > 0) {
                    view.lookupReference('removeToSeed').enable();
                }
                else {
                    view.lookupReference('removeToSeed').disable();
                }
            }
        });

        view.lookupReference('addToSeed').on({
            click: function() {
                let metaboliteTable = view.lookupReference('metaboliteTableGir');
                let seedTable = view.lookupReference('seedTableGir');

                let seedTableStore = seedTable.getStore();
                let metaboliteTableStore = metaboliteTable.getStore();

                let metabolites = metaboliteTable.getSelectionModel().selected.items;

                while(metabolites.length > 0) {
                    let newSeed = {seed: metabolites[0].data.metabolite};
                    let oldMetabolite = {metabolite: metabolites[0].data.metabolite};

                    seedTableStore.add(newSeed);
                    metaboliteTableStore.removeAt(metaboliteTableStore.find('metabolite', oldMetabolite.metabolite));
                }
            }
        });

        view.lookupReference('removeToSeed').on({
            click: function() {
                let metaboliteTable = view.lookupReference('metaboliteTableGir');
                let seedTable = view.lookupReference('seedTableGir');

                let seedTableStore = seedTable.getStore();
                let metaboliteTableStore = metaboliteTable.getStore();

                let seeds = seedTable.getSelectionModel().selected.items;

                while(seeds.length > 0) {
                    let oldSeed = {seed: seeds[0].data.seed};
                    let newMetabolite = {metabolite: seeds[0].data.seed};

                    seedTableStore.removeAt(seedTableStore.find('seed', oldSeed.seed));
                    metaboliteTableStore.add(newMetabolite);
                }
            }
        })

        view.lookupReference('startNetEx').on({
            click: function() {
                // get selected seeds
                let listMi = [];
                let seedTableStore = view.lookupReference('seedTableGir').getStore();
                let seeds = seedTableStore.getData().items;

                seeds.forEach(function(seed) {
                    let thisSeed = seed.data.seed;
                    listMi.push(thisSeed);
                });

                if (listMi.length !== 0) {
                    // change panel: params <-> manager
                    metExploreD3.fireEventArg("netEx", "switchPanel", "manager");

                    // start NetEx
                    metExploreD3.GraphRank.launchGIR = true;
                    metExploreD3.GraphRank.startGir(listMi);
                }
                else {
                    metExploreD3.displayWarning("Seeds not selected", "To use Network Explorer, you must import rank file and select one or more seed(s)");
                }
            }
        });

        view.lookupReference('quitNetEx').on({
            click: function() {
                // change panel: params <-> manager
                metExploreD3.fireEventArg("netEx", "switchPanel", "params");

                // stop NetEx
                metExploreD3.GraphRank.launchGIR = false;
                metExploreD3.GraphRank.quitGir();
            }
        })

        view.lookupReference('loadRankFile').on({
            change: function() {
                metExploreD3.GraphUtils.handleFileSelect(view.lookupReference('loadRankFile').fileInputEl.dom, me.loadData);
                view.lookupReference('loadRankFile').fileInputEl.dom.value = "";
            }
        });

        view.lookupReference('metaboliteSearch').on({
            change: function() {
                let searchValue = view.lookupReference('metaboliteSearch').getValue();
                if (searchValue !== "") {
                    let metaboliteStoreFilter = view.lookupReference('metaboliteTableGir').getStore().getFilters("metabolite");
                    metaboliteStoreFilter.add({property: 'metabolite',value: searchValue, operator: 'like'});
                }
                else {
                    let metaboliteStoreFilter = view.lookupReference('metaboliteTableGir').getStore().getFilters("metabolite");
                    let filter = metaboliteStoreFilter.items;
                    metaboliteStoreFilter.remove(filter);
                }
            }
        });

        view.lookupReference('extractSubNetwork').on({
            click: function() {
                if (metExploreD3.GraphRank.launchGIR){
                    let extract = metExploreD3.GraphRank.quitAndExtract();

                    if (extract){
                        metExploreD3.fireEventArg("netEx", "switchPanel", "params");
                        metExploreD3.GraphRank.launchGIR = false;
                    }
                }
            }
        });
    },

    loadData : function(tabTxt, title) {
        let mask = metExploreD3.createLoadMask("Loading data","viz");

		let data = tabTxt;
		tabTxt = tabTxt.replace(/\r/g, "");
	    let lines = tabTxt.split('\n');

	    let firstLine = lines.splice(0, 1);
	    firstLine = firstLine[0].split('\t');

	    let targetName = firstLine.splice(0, 1);
	    let array = [];

        for (let i = 0; i < _metExploreViz.rank.length; i++){
            if (title === _metExploreViz.rank[i].name){
                metExploreD3.displayWarning("Loaded file", "This file has already been loaded");
                return;
            }
        }

		if(targetName[0]=="Identifier" || targetName[0]=="reactionId" || targetName[0]=="Name") {
            metExploreD3.showMask(mask);
            metExploreD3.deferFunction(
                function(){
                    for (let i = lines.length - 1; i >= 0; i--) {
            	    	lines[i] = lines[i].split('\t').map(function (val) {
            				return val.replace(",", ".");
            			});
                        if (lines[i].length === 1){
                            lines.pop(i);
                        }
            	    }

                    let rankScore = {};
                    lines.map(function(line, i){
                        if (line[2] === undefined){
                            rankScore[line[0]] = [line[1], "10000"];
                        }
                        if (line[1] === undefined && line[2] === undefined){
                            rankScore[line[0]] = ["10000", "10000"];
                        }
                        if (line[1] !== undefined && line[2] !== undefined) {
                            rankScore[line[0]] = [line[1],line[2]];
                        }
                    });
                    let data = metExploreD3.GraphRank.saveNetwork();
                    let rank = new Rank(title, data, "rankData", rankScore);

                    _metExploreViz.addRank(rank);
                    metExploreD3.fireEvent("netEx","fileLoad");

                    metExploreD3.hideMask(mask);
                }, 100);
        }
        else {
			// Warning for bad syntax file
			metExploreD3.displayWarning("Syntaxe error",
                'File have bad syntax. See <a target="_blank" href="https://metexplore.toulouse.inrae.fr/metexplore-viz-doc/documentation/import">MetExploreViz documentation</a>.'
            );
		}
	},

    parseFile: function(rankData) {
        let view = this.getView();

        let rankScore = rankData.rank;
        let allMetabolite = Object.keys(rankScore);
        let listMi = [];
        let listSeed = [];

        allMetabolite.forEach(function(data){
            let rankIn = parseInt(rankScore[data][0]);
            let rankOut = parseInt(rankScore[data][1]);
            if (rankIn < 3 || rankOut < 3) {
                let nodeName = metExploreD3.GraphRank.transformId(data, "name");

                if (nodeName !== undefined){
                    let mi = {seed: nodeName};
                    listSeed.push(mi);
                }
            }
            else {
                let nodeName = metExploreD3.GraphRank.transformId(data, "name");

                if (nodeName !== undefined){
                    let mi = {metabolite: nodeName};
                    listMi.push(mi);
                }
            }
        });

        let metaboliteTable = view.lookupReference('metaboliteTableGir');
        let seedTable = view.lookupReference('seedTableGir');

        let seedTableStore = seedTable.getStore();
        let metaboliteTableStore = metaboliteTable.getStore();

        seedTableStore.setData(listSeed);
        metaboliteTableStore.setData(listMi);
    }
});
