/**
 * @author JCG
 * (a)description GirForm : Manage GIR
 */
Ext.define('metExploreViz.view.form.girForm.GirForm', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.girForm',
    requires: [
        "metExploreViz.view.form.girForm.GirFormController"
    ],
    controller: "form-girForm-girForm",
    layout:{
       type:'vbox',
       align:'stretch'
    },
    region:'north',
    width:'100%',
    margins:'0 0 0 0',
    split:true,
    animation: true,
    autoScroll: true,
    autoHeight: true,
    id: "girID",

    items: [
        // Params panel: before launch NetEx -----------------------------------
        {
            // load file
            border: false,
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'stretch'
            },
            margin: '20 0 0 0',
            reference: 'panelLoadRankFile',
            items: [
                {
                    xtype: 'textfield',
                    emptyText: '-- Import rank score file --',
                    margin: '5 5 5 5',
                    width: '75%',
                    reference: 'selectFile',
                    editable: false
                },
                {
                    xtype: 'filefield',
                    buttonOnly: true,
                    buttonText: 'import file',
                    width: '25%',
                    margin: '5 5 5 5',
                    reference: 'loadRankFile'
                }
            ]
        },

        {
            // fuzzy search
            xtype: 'textfield',
            emptyText: '-- Filter metabolites table --',
            margin: '5 5 5 5',
            width: '75%',
            reference: 'metaboliteSearch',
            hidden: true
        },

        {
            // two table
            border: false,
            xtype:'panel',
            layout:{
                type:'hbox',
                align: 'stretch'
            },
            width: '100%',
            height: 300,
            reference: 'gridSeedSelection',
            hidden: true,
            items: [
                {
                    // metabolites table
                    xtype: 'gridpanel',
                    reference: 'metaboliteTableGir',
                    selModel: {
                        selType: 'rowmodel',
                        mode: 'SIMPLE'
                    },
                    plugins: 'gridfilters',
                    store: {
                        fields: ['metabolite'],
                        sorters: [{
                         	property: 'metabolite',
                         	direction: 'ASC'
                       	}]
                    },
                    columns: [
                        {
                            text: 'Metabolites',
                            dataIndex: 'metabolite',
                            flex: 2,
                            sortable: true,
                            filter: {
                                type: 'string'
                            }
                        }
                    ],
                    margin: '5 5 5 5',
                    width: '50%',
                    height: '100%'
                },
                {
                    // seeds table
                    xtype: 'grid',
                    reference: 'seedTableGir',
                    selModel: {
                        selType: 'rowmodel',
                        mode: 'SIMPLE'
                    },
                    store: {
                        fields: ['seed'],
                        sorters: [{
                         	property: 'seed',
                         	direction: 'ASC'
                       	}]
                    },
                    columns: [{
                        text: 'Seeds',
                        dataIndex: 'seed',
                        flex: 2,
                        sortable: true
                    }],
                    margin: '5 5 5 5',
                    width: '50%',
                    height: '100%'
                }
            ]
        },

        {
            // add / remove buttons
            xtype: 'panel',
            reference: 'addNRemoveButtons',
            hidden: true,
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'button',
                    html: 'ADD',
                    width: '50%',
                    margin: '5 5 5 5',
                    disabled: true,
                    reference: 'addToSeed',
                    tooltip: 'Add selected metabolite(s) to seed(s)'
                },
                {
                    xtype: 'button',
                    html: 'REMOVE',
                    width: '50%',
                    margin: '5 5 5 5',
                    disabled: true,
                    reference: 'removeToSeed',
                    tooltip: 'Remove selected metabolite(s) from seed(s)'
                }
            ]
        },

        {
            // start button
            xtype: 'button',
            html: 'START',
            margin: '5 5 5 5',
            reference: 'startNetEx',
            hidden: true,
            tooltip: 'Start network explorer'
        },

        // Manage panel: after launch NetEx ------------------------------------
        {
            // caption panel
            xtype: 'panel',
            width: '100%',
            height: 200,
            hidden: true,
            border: true,
            reference: 'netExCaption',
            items: [
                {
                    xtype: 'label',
                    html: '<p>Panel caption gir WIP...</p>'
                }
            ]
        },
        {
            // two table
            border: false,
            xtype:'panel',
            hidden: true,
            layout:{
                type:'hbox',
                align: 'stretch'
            },
            width: '100%',
            height: 300,
            reference: 'gridDataDisplay',
            items: [
                {
                    xtype: 'tabpanel',
                    width: '50%',
                	height: '100%',
                	split:true,
                	closable: false,
                    hidden: false,
                	region: 'west',
                	tabBar:{
                		cls:"vizTBar"
                	},
                    items: [
                        {
                            // metabolites table
                            title: 'Metabolites',
                            xtype: 'gridpanel',
                            reference: 'metaboliteDataTable',
                            hideHeaders: true,
                            border: true,
                            store: {
                                fields: ['metabolite'],
                                sorters: [{
                                 	property: 'metabolite',
                                 	direction: 'ASC'
                               	}],
                            },
                            columns: [
                                {
                                    dataIndex: 'metabolite',
                                    flex: 2,
                                    sortable: true
                                }
                            ],
                            // margin: '5 5 5 5',
                            width: '50%',
                            height: '100%'
                        },
                        {
                            // reactions table
                            title: 'Reactions',
                            xtype: 'gridpanel',
                            reference: 'reactionDataTable',
                            hideHeaders: true,
                            border: true,
                            store: {
                                fields: ['reaction'],
                                sorters: [{
                                 	property: 'reaction',
                                 	direction: 'ASC'
                               	}],
                            },
                            columns: [
                                {
                                    dataIndex: 'reaction',
                                    flex: 2,
                                    sortable: true
                                }
                            ],
                            // margin: '5 5 5 5',
                            width: '50%',
                            height: '100%'
                        }
                    ]
                },
                {
                    xtype: 'tabpanel',
                    width: '50%',
                	height: '100%',
                	split:true,
                	closable: false,
                    hidden: false,
                	region: 'west',
                	tabBar:{
                		cls:"vizTBar"
                	},
                    items: [
                        {
                            // pathways table
                            title: 'Pathways',
                            xtype: 'gridpanel',
                            reference: 'pathwayDataTable',
                            hideHeaders: true,
                            border: true,
                            store: {
                                fields: ['pathway'],
                                sorters: [{
                                 	property: 'pathway',
                                 	direction: 'ASC'
                               	}],
                            },
                            columns: [
                                {
                                    dataIndex: 'pathway',
                                    flex: 2,
                                    sortable: true
                                }
                            ],
                            // margin: '5 5 5 5',
                            width: '100%',
                            height: '100%'
                        },
                        {
                            // compartments table
                            title: 'Compartments',
                            xtype: 'gridpanel',
                            reference: 'compartmentDataTable',
                            hideHeaders: true,
                            border: true,
                            store: {
                                fields: ['compartment'],
                                sorters: [{
                                 	property: 'compartment',
                                 	direction: 'ASC'
                               	}],
                            },
                            columns: [
                                {
                                    dataIndex: 'compartment',
                                    flex: 2,
                                    sortable: true
                                }
                            ],
                            // margin: '5 5 5 5',
                            width: '100%',
                            height: '100%'
                        }
                    ]
                }
            ]
        },
        {
            // extract / quit buttons
            xtype: 'panel',
            hidden: true,
            reference: 'extractNQuitButtons',
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'button',
                    html: 'EXTRACT',
                    width: '50%',
                    margin: '5 5 5 5',
                    reference: 'extractSubNetwork',
                    tooltip: 'Extract this subnetwork'
                },
                {
                    xtype: 'button',
                    html: 'QUIT',
                    width: '50%',
                    margin: '5 5 5 5',
                    reference: 'quitNetEx',
                    tooltip: 'Quit NetEx and restore previous network'
                }
            ]
        }
    ]
});
