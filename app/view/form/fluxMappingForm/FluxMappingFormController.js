/**
 * @author JCG
 * (a)description fluxMappingFormController : Control displaying flux value
 */

Ext.define('metExploreViz.view.form.fluxMappingForm.FluxMappingFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.form-fluxMappingForm-fluxMappingForm',
    requires: [
        "metExploreViz.view.form.fluxScaleEditor.FluxScaleEditor"
    ],

    /**
     * Aplies event linsteners to the view
     */
    init:function(){
        var me 		= this,
            viewModel   = me.getViewModel(),
            view      	= me.getView();

        // Regex to remove bad chars in dom ids
        me.regexpPanel=/\.|>|<| |,|\/|=|\(|\)/g;


        view.on({
            fileParse: function(){
                me.fileParse();
            },
            fileLoad: function(){
                view.enable();
            },
            updateScaleEditor: function(){
                me.updateScaleEditor();
            },
            resetScale: function(cond, svgView){
                me.resetScale(cond, svgView)
            }
        });

        view.lookupReference('selectFile').on({
            change: function(){
                var selectFile = this.getValue();
                var nbCol = view.lookupReference('selectColNumber').getValue();
                var scaleSelector = view.lookupReference('scaleSelector').getValue();
                if (scaleSelector === "Manual"){
                    d3.select(view.lookupReference('scaleEditor').el.dom).select("#scaleEditor")
                        .selectAll("*").remove();
                    d3.select(view.lookupReference('scaleEditor2').el.dom).select("#scaleEditor2")
                        .selectAll("*").remove();
                    view.lookupReference('scaleEditor').setHidden(true);
                    view.lookupReference('scaleEditor2').setHidden(true);
                    view.lookupReference('scaleEditorLabel1').setHidden(true);
                    view.lookupReference('scaleEditorLabel2').setHidden(true);
                }
                view.lookupReference('selectConditionFlux').setValue("-- Select Condition --");
                view.lookupReference('selectConditionFlux2').setValue("-- Select Condition --");
                me.colParse(2, selectFile);
            }
        });

        view.lookupReference('selectColNumber').on({
            change: function(){
                if (view.lookupReference('selectColNumber').getValue() === "two"){
                    view.lookupReference("secondConditionLabel").setHidden(false);
                    view.lookupReference("firstConditionLabel").setHidden(false);
                    view.lookupReference("secondConditionBox").setHidden(false);
                    if (view.lookupReference('scaleSelector').getValue() === "Manual"){
                        var condSelect = view.lookupReference('selectConditionFlux').getValue();
                        var condSelect2 = view.lookupReference('selectConditionFlux2').getValue();
                        var selectedFile = view.lookupReference('selectFile').getValue();
                        me.drawScaleEditor(selectedFile, condSelect, condSelect2, "two");
                    }
                }
                if (view.lookupReference('selectColNumber').getValue() === "one"){
                    view.lookupReference("secondConditionLabel").setHidden(true);
                    view.lookupReference("firstConditionLabel").setHidden(true);
                    view.lookupReference("secondConditionBox").setHidden(true);
                    if (view.lookupReference('scaleSelector').getValue() === "Manual"){
                        var condSelect = view.lookupReference('selectConditionFlux').getValue();
                        var selectedFile = view.lookupReference('selectFile').getValue();
                        me.drawScaleEditor(selectedFile, condSelect, "null", "one");
                    }
                }
                if (view.lookupReference('selectFile').getValue() !== null){
                    if (view.lookupReference('selectColNumber').getValue() === "one"){
                        var selectedFile = view.lookupReference("selectFile").getValue();
                        me.colParse(1, selectedFile);
                    }
                    if (view.lookupReference('selectColNumber').getValue() === "two"){
                        var selectedFile = view.lookupReference("selectFile").getValue();
                        me.colParse(2, selectedFile);
                    }
                }
            }
        });

        view.lookupReference('runFluxVizu').on({
            click: function(){
                me.runButtonFunction();
            }
        });

        view.lookupReference('runNewParams').on({
            click: function(){
                metExploreD3.GraphStyleEdition.fluxPath1 = false;
                metExploreD3.GraphStyleEdition.fluxPath2 = false;
                metExploreD3.GraphLink.tick('viz');
                metExploreD3.GraphCaption.drawCaption();
                metExploreD3.GraphFlux.restoreStyles(_metExploreViz.linkStyle);
                metExploreD3.GraphFlux.removeGraphDistrib();
                metExploreD3.GraphFlux.removeValueOnEdge();

                var selectedFile = view.lookupReference('selectFile').getValue();
                var nbColSelect = view.lookupReference('selectColNumber').getValue();
                var condSelect = view.lookupReference('selectConditionFlux').getValue();
                var switchGraph = view.lookupReference('displayGraphDistrib').getValue();
                var scaleSelector = view.lookupReference('scaleSelector').getValue();

                if (selectedFile !== null && nbColSelect !== null && condSelect !== null){
                    if (nbColSelect === "one"){
                        var color = document.getElementById("html5colorpickerFlux1").value;
                        metExploreD3.GraphStyleEdition.fluxPath1 = true;
                        me.computeFlux(selectedFile, nbColSelect, condSelect, "null", color, switchGraph, scaleSelector);
                    }
                    if (nbColSelect === "two"){
                        var color = [document.getElementById("html5colorpickerFlux1").value,
                                    document.getElementById("html5colorpickerFlux2").value];
                        metExploreD3.GraphStyleEdition.fluxPath2 = true;
                        var condSelect2 = view.lookupReference('selectConditionFlux2').getValue();
                        me.computeFlux(selectedFile, nbColSelect, condSelect, condSelect2, color, switchGraph, scaleSelector);
                    }
                    if (view.lookupReference('addValueNetwork').getValue() === true){
                        var size = view.lookupReference('fontSize').getValue();
                        var label = view.lookupReference('selectLabelDisplayed').getValue();
                        var sdOn = view.lookupReference('addSdNetwork').getValue();
                        me.applyCaptionOnEdge(size, label, sdOn);
                    }
                }
            }
        });

        view.lookupReference('addValueNetwork').on({
            change: function(){
                if (metExploreD3.GraphStyleEdition.fluxPath1 === true || metExploreD3.GraphStyleEdition.fluxPath2 === true){
                    if (view.lookupReference('addValueNetwork').getValue() === true){
                        var size = view.lookupReference('fontSize').getValue();
                        var label = view.lookupReference('selectLabelDisplayed').getValue();
                        var sdOn = view.lookupReference('addSdNetwork').getValue();
                        me.applyCaptionOnEdge(size, label, sdOn);
                    }
                    if (view.lookupReference('addValueNetwork').getValue() === false){
                        metExploreD3.GraphFlux.removeValueOnEdge();
                    }
                }
                if (view.lookupReference('addValueNetwork').getValue() === true){
                    view.lookupReference('fontSize').setHidden(false);
                    view.lookupReference('selectLabel').setHidden(false);
                    view.lookupReference('addSdNetwork').setHidden(false);
                }
                if (view.lookupReference('addValueNetwork').getValue() === false){
                    view.lookupReference('fontSize').setHidden(true);
                    view.lookupReference('selectLabel').setHidden(true);
                    view.lookupReference('addSdNetwork').setHidden(true);
                }
            }
        });

        view.lookupReference('addSdNetwork').on({
            change: function(){
                if (metExploreD3.GraphStyleEdition.fluxPath1 === true || metExploreD3.GraphStyleEdition.fluxPath2 === true){
                    metExploreD3.GraphFlux.removeValueOnEdge();
                    var size = view.lookupReference('fontSize').getValue();
                    var label = view.lookupReference('selectLabelDisplayed').getValue();
                    var sdOn = view.lookupReference('addSdNetwork').getValue();
                    me.applyCaptionOnEdge(size, label, sdOn);
                }
            }
        });

        view.lookupReference('fontSize').on({
            keypress: function(field, event){
                if (event.getKey() === event.ENTER){
                    var size = view.lookupReference('fontSize').getValue();
                    metExploreD3.GraphFlux.setFontSize(size);
                }
            }
        });

        view.lookupReference('selectLabelDisplayed').on({
            change: function(){
                if (metExploreD3.GraphStyleEdition.fluxPath1 === true || metExploreD3.GraphStyleEdition.fluxPath2 === true){
                    if (view.lookupReference('addValueNetwork').getValue() === true){
                        metExploreD3.GraphFlux.removeValueOnEdge();
                        var size = view.lookupReference('fontSize').getValue();
                        var label = view.lookupReference('selectLabelDisplayed').getValue();
                        var sdOn = view.lookupReference('addSdNetwork').getValue();
                        me.applyCaptionOnEdge(size, label, sdOn);
                    }
                }
            }
        });

        view.lookupReference('scaleSelector').on({
            change: function(){
                if (this.getValue() === "Manual"){
                    var nbCol = view.lookupReference('selectColNumber').getValue();
                    if (nbCol === "one"){
                        var condSelect = view.lookupReference('selectConditionFlux').getValue();
                        if (condSelect !== null){
                            view.lookupReference('scaleEditor').setHidden(false);
                            var selectedFile = view.lookupReference('selectFile').getValue();
                            me.drawScaleEditor(selectedFile, condSelect, "null", nbCol);
                        }
                    }
                    if (nbCol === "two"){
                        var condSelect = view.lookupReference('selectConditionFlux').getValue();
                        var condSelect2 = view.lookupReference('selectConditionFlux2').getValue();
                        if (condSelect !== null && condSelect2 !== null){
                            view.lookupReference('scaleEditor').setHidden(false);
                            var selectedFile = view.lookupReference('selectFile').getValue();
                            me.drawScaleEditor(selectedFile, condSelect, condSelect2, nbCol);
                        }
                    }
                }
                if (this.getValue() !== "Manual"){
                    view.lookupReference('scaleEditor').setHidden(true);
                }
            }
        });

        view.lookupReference('selectConditionFlux').on({
            change: function(){
                if (view.lookupReference('scaleSelector').getValue() === "Manual"){
                    view.lookupReference('scaleEditor').setHidden(false);

                    var condSelect = view.lookupReference('selectConditionFlux').getValue();
                    var condSelect2 = view.lookupReference('selectConditionFlux2').getValue();
                    var selectedFile = view.lookupReference('selectFile').getValue();
                    var nbColSelect = view.lookupReference('selectColNumber').getValue();
                    if (condSelect2 === null && condSelect !== null){
                        me.drawScaleEditor(selectedFile, condSelect, "null", nbColSelect);
                    }
                    if (condSelect2 !== null && condSelect !== null){
                        me.drawScaleEditor(selectedFile, condSelect, condSelect2, nbColSelect);
                    }
                }
            }
        });

        view.lookupReference('selectConditionFlux2').on({
            change: function(){
                if (view.lookupReference('scaleSelector').getValue() === "Manual"){

                    var condSelect = view.lookupReference('selectConditionFlux').getValue();
                    var condSelect2 = view.lookupReference('selectConditionFlux2').getValue();
                    var selectedFile = view.lookupReference('selectFile').getValue();
                    var nbColSelect = view.lookupReference('selectColNumber').getValue();
                    if (condSelect2 !== null && condSelect === null){
                        me.drawScaleEditor(selectedFile, "null", condSelect2, nbColSelect);
                    }
                    if (condSelect2 !== null && condSelect !== null){
                        me.drawScaleEditor(selectedFile, condSelect, condSelect2, nbColSelect);
                    }
                    me.drawScaleEditor(selectedFile, condSelect, condSelect2, nbColSelect);
                }
            }
        });
    },

    runButtonFunction: function() {
        var me = this;
        var view = this.getView();
        if (metExploreD3.GraphStyleEdition.fluxPath1 === false && metExploreD3.GraphStyleEdition.fluxPath2 === false){
            var selectedFile = view.lookupReference('selectFile').getValue();
            var nbColSelect = view.lookupReference('selectColNumber').getValue();
            var condSelect = view.lookupReference('selectConditionFlux').getValue();
            var switchGraph = view.lookupReference('displayGraphDistrib').getValue();
            var scaleSelector = view.lookupReference('scaleSelector').getValue();

            if (selectedFile !== null && nbColSelect !== null && condSelect !== null){
                if (nbColSelect === "one"){
                    var color = document.getElementById("html5colorpickerFlux1").value;
                    metExploreD3.GraphStyleEdition.fluxPath1 = true;
                    me.computeFlux(selectedFile, nbColSelect, condSelect, "null", color, switchGraph, scaleSelector);
                }
                if (nbColSelect === "two"){
                    var color = [document.getElementById("html5colorpickerFlux1").value,
                                document.getElementById("html5colorpickerFlux2").value];
                    metExploreD3.GraphStyleEdition.fluxPath2 = true;
                    var condSelect2 = view.lookupReference('selectConditionFlux2').getValue();
                    me.computeFlux(selectedFile, nbColSelect, condSelect, condSelect2, color, switchGraph, scaleSelector);
                }
                if (view.lookupReference('addValueNetwork').getValue() === true){
                    var size = view.lookupReference('fontSize').getValue();
                    var label = view.lookupReference('selectLabelDisplayed').getValue();
                    var sdOn = view.lookupReference('addSdNetwork').getValue();
                    me.applyCaptionOnEdge(size, label, sdOn);
                }
                view.lookupReference('runNewParams').enable();
                view.lookupReference('runFluxVizu').setText("Remove display");
            }
        }

        else{
            metExploreD3.GraphStyleEdition.fluxPath1 = false;
            metExploreD3.GraphStyleEdition.fluxPath2 = false;
            metExploreD3.GraphLink.tick('viz');
            metExploreD3.GraphCaption.drawCaption();
            metExploreD3.GraphFlux.restoreStyles(_metExploreViz.linkStyle);
            metExploreD3.GraphFlux.removeGraphDistrib();
            metExploreD3.GraphFlux.removeValueOnEdge();

            view.lookupReference('runNewParams').disable();
            view.lookupReference('runFluxVizu').setText("Display");
        }
    },

    applyCaptionOnEdge: function(size, label, sdOn) {
        if (sdOn){
            var view = this.getView();
            var selectedFile = view.lookupReference('selectFile').getValue();
            var nbColSelect = view.lookupReference('selectColNumber').getValue();
            if (nbColSelect === 'one') {
                var select = view.lookupReference('selectConditionFlux').getValue();
                var condSelect = 'sd_'+select;
                var sdData = this.getFluxData(selectedFile, "one", condSelect, "null", "sd");
                metExploreD3.GraphFlux.addValueOnEdge(size, label, sdData);
            } else {
                var select1 = view.lookupReference('selectConditionFlux').getValue();
                var condSelect1 = 'sd_'+select1;
                var select2 = view.lookupReference('selectConditionFlux2').getValue();
                var condSelect2 = 'sd_'+select2;

                var sdData = this.getFluxData(selectedFile, "two", condSelect1, condSelect2, "sd");
                metExploreD3.GraphFlux.addValueOnEdge(size, label, sdData);
            }
        }
        else {
            metExploreD3.GraphFlux.addValueOnEdge(size, label);
        }
    },

    /**
     * Parse selected file and fill one or two combobox with conditions
     * @param {String} nbCol number of value to visualise
     * @param {String} selectedFile file to parse
     */
    colParse: function(nbCol, selectedFile){
        var fluxList = _metExploreViz.flux;
        var fileIndex = [];
        fluxList.forEach(function(list, i){
            if (list.name === selectedFile){
                fileIndex.push(i);
            }
        });
        var fluxCondition = fluxList[fileIndex].conditions;

        var listCond = [];

        for (var i = 0; i < fluxCondition.length; i++){
            var cond = fluxCondition[i];
            var tmp = {fluxCond:cond};
            listCond.push(tmp);
        }

        if (nbCol === 1){
            var comboComponent = this.getView().lookupReference('selectConditionFlux');
            var condStore = comboComponent.getStore();

            condStore.setData(listCond);
        }
        if (nbCol === 2){
            var comboComponent = this.getView().lookupReference('selectConditionFlux');
            var comboComponent2 = this.getView().lookupReference('selectConditionFlux2');
            var condStore = comboComponent.getStore();
            var condStore2 = comboComponent2.getStore();

            condStore.setData(listCond);
            condStore2.setData(listCond);
        }
    },

    /**
     * Parse flux object and fill combobox to select file
     */
    fileParse: function(){
        var fluxList = _metExploreViz.flux;
        var fileName = [];

        fluxList.forEach(function(flux){
            fileName.push({'file':flux.name});
        });

        var comboComponent = this.getView().lookupReference('selectFile');
        var condStore = comboComponent.getStore();

        condStore.setData(fileName);
    },

    /**
     * Regroup params to launch display flux visualisation and ditribution graph functions
     * @param {String} selectedFile file to parse
     * @param {String} nbCol number of value to visualise
     * @param {String} condSelect first condition name
     * @param {String} condSelect2 second condition name
     * @param {String} color hexadecimal to color condition
     * @param {Boolean} switchGraph swtich between all value or display value for distribution graph
     * @param {String} scaleSelector scale mod select
     */
    computeFlux: function(selectedFile, nbCol, condSelect, condSelect2, color, switchGraph, scaleSelector){
        var me = this;
        var view = me.getView();

        var data = this.getFluxData(selectedFile, nbCol, condSelect, condSelect2, "flux");
        var conData = data[0];
        var targetLabel = data[1];
        var scaleRange1 = [];

        var scaleSave = view[selectedFile];

        if (scaleSave !== undefined){
            if (nbCol === "one"){
                var scaleRange1 = scaleSave[condSelect];
            }
            if (nbCol === "two"){
                var scaleRange1 = scaleSave[condSelect+condSelect2];
                if (scaleRange1 === undefined){
                    var scaleRange1 = scaleSave[condSelect2+condSelect];
                }
            }
        }

        metExploreD3.GraphFlux.displayChoice(conData, targetLabel, nbCol, color, scaleSelector, scaleRange1);

        if (nbCol === "one"){
            metExploreD3.GraphFlux.graphDistribOne(conData, color, switchGraph, scaleSelector, scaleRange1);
        }
        if (nbCol === "two"){
            metExploreD3.GraphFlux.graphDistribTwo(conData, color, switchGraph, scaleSelector, scaleRange1);
        }
    },

    /**
     * Get flux values from selected file and conditions
     * @param {String} selectedFile file to parse
     * @param {String} nbCol number of condition to display
     * @param {String} condSelect first condition name
     * @param {String} condSelect2 second condition name
     * @param {String} type sd or flux
     * @returns {Array}
     */
    getFluxData: function(selectedFile, nbCol, condSelect, condSelect2, type){
        var fluxList = _metExploreViz.flux;
        var fileIndex = [];
        fluxList.forEach(function(list, i){
            if (list.name === selectedFile){
                fileIndex.push(i);
            }
        });

        var targetLabel = _metExploreViz.flux[fileIndex].targetLabel;
        if (type === "flux"){
            var fluxData = _metExploreViz.flux[fileIndex].data;
            var fluxCond = _metExploreViz.flux[fileIndex].conditions;
        }
        if (type === "sd"){
            var fluxData = _metExploreViz.flux[fileIndex].sdData;
            var fluxCond = _metExploreViz.flux[fileIndex].sdConditions;
        }
        var conData = [];

        if (nbCol === "one"){
            for (var i = 0; i < fluxCond.length; i++){
                if (fluxCond[i].includes(condSelect)){
                    var indexCond = i+1;
                }
            }
            for (var i = 0; i < fluxData.length; i++){
                var data = [];
                data.push(fluxData[i][0]);
                data.push(fluxData[i][indexCond]);
                conData.push(data)
            }
        }

        if (nbCol === "two"){
            var condSplit = [condSelect, condSelect2];
            var indexCond1;
            var indexCond2;

            for (var i = 0; i < fluxCond.length; i++){
                if (fluxCond[i].includes(condSplit[0])){
                    indexCond1 = i+1;
                }
                if (fluxCond[i].includes(condSplit[1])){
                    indexCond2 = i+1;
                }
            }
            for (var i = 0; i < fluxData.length; i++){
                var data = [];
                data.push(fluxData[i][0]);
                data.push(fluxData[i][indexCond1]);
                data.push(fluxData[i][indexCond2]);
                conData.push(data);
            }
        }
        var fluxData;
        if (type === "flux") {
            fluxData = [conData, targetLabel];
        }
        if (type === "sd") {
            var sdData = {};
            conData.forEach(function(reaction) {
                if (nbCol === "one") {
                    sdData[reaction[0]] = [reaction[1]];
                } else {
                    sdData[reaction[0]] = [reaction[1], reaction[2]];
                }
            });
            fluxData = sdData;
        }
        return fluxData;
    },

    /**
     * Draw scale graph
     * @param {String} selectedFile file to parse
     * @param {String} selectedCond first condition name
     * @param {String} selectedCond2 second condition name
     * @param {String} nbCol number of condition to display
     */
    drawScaleEditor: function(selectedFile, selectedCond, selectedCond2, nbCol) {
		var me = this;
		var view = me.getView();

		var margin = 0;
		var width = 190;
		var height = 50;

        var data = this.getFluxData(selectedFile, nbCol, selectedCond, selectedCond2, "flux");
        var fluxData = data[0];
        var targetLabel = data[1];

        var dataSave = view[selectedFile];
        if (dataSave === undefined){
            dataSave = {};
        }

        if (nbCol === "two"){
            var fluxData1 = [];
            for (var i = 0; i < fluxData.length; i++){
                var tmp = [fluxData[i][0], fluxData[i][1]];
                fluxData1.push(tmp);
                var tmp = [fluxData[i][0], fluxData[i][2]];
                fluxData1.push(tmp);
            }
            var scaleRange1 = metExploreD3.GraphFlux.getScale(fluxData1, targetLabel);
            if (dataSave[selectedCond+selectedCond2] !== undefined){
                scaleRange1 = dataSave[selectedCond+selectedCond2];
            }
            if (dataSave[selectedCond2+selectedCond] !== undefined){
                scaleRange1 = dataSave[selectedCond2+selectedCond];
            }
            if (dataSave[selectedCond+selectedCond2] === undefined && dataSave[selectedCond2+selectedCond] === undefined) {
                dataSave[selectedCond+selectedCond2] = scaleRange1;
            }

            view[selectedFile] = dataSave;

            if (scaleRange1 !== undefined){
                var svg = d3.select(view.lookupReference('scaleEditor').el.dom).select("#scaleEditor");

        		metExploreD3.GraphNumberScaleEditor.createNumberScaleCaption(svg, width, height, margin, scaleRange1);

        		svg.on("click", function(){
        			var win = Ext.create("metExploreViz.view.form.fluxScaleEditor.FluxScaleEditor", {
                        scaleRange: scaleRange1,
                        cond: "first"
                    });

        			win.show();
        		});
            }
        }

        if (nbCol === "one"){
            var scaleRange = metExploreD3.GraphFlux.getScale(fluxData, targetLabel);

            if (dataSave[selectedCond] !== undefined){
                scaleRange = dataSave[selectedCond];
            }
            else {
                dataSave[selectedCond] = scaleRange;
            }

            view[selectedFile] = dataSave;

            if (scaleRange !== undefined){
                var svg = d3.select(view.lookupReference('scaleEditor').el.dom).select("#scaleEditor");

        		metExploreD3.GraphNumberScaleEditor.createNumberScaleCaption(svg, width, height, margin, scaleRange);

        		svg.on("click", function(){
        			var win = Ext.create("metExploreViz.view.form.fluxScaleEditor.FluxScaleEditor", {
                        scaleRange: scaleRange,
                        cond: "first"
                    });

        			win.show();
        		});
            }
        }

    },

    /**
     * Update scale graph
     */
    updateScaleEditor: function(){
        var me = this;
        var view = me.getView();

        var margin = 0;
		var width = 190;
		var height = 50;

        var selectedFile = view.lookupReference('selectFile').getValue();

        var scaleRange = Ext.getCmp("fluxScaleEditorID").scaleRange;
        var cond = Ext.getCmp("fluxScaleEditorID").cond;

        if (cond === "first"){
            var selectedCond = view.lookupReference('selectConditionFlux').getValue();

            var dataSave = view[selectedFile];
            if (dataSave !== undefined){
                dataSave[selectedCond] = scaleRange;
            }
            else {
                dataSave = {};
                dataSave[selectedCond] = scaleRange;
            }
            view[selectedFile] = dataSave;

            var svg = d3.select(view.lookupReference('scaleEditor').el.dom).select("#scaleEditor");
            svg.selectAll("*").remove();

            svg = d3.select(view.lookupReference('scaleEditor').el.dom).select("#scaleEditor");

            metExploreD3.GraphNumberScaleEditor.createNumberScaleCaption(svg, width, height, margin, scaleRange);

            svg.on("click", function(){
    			var win = Ext.create("metExploreViz.view.form.fluxScaleEditor.FluxScaleEditor", {
                    scaleRange: scaleRange,
                    cond: 'first'
                });

    			win.show();
    		});
        }

        if (cond === "second"){
            var selectedCond = view.lookupReference('selectConditionFlux2').getValue();

            var dataSave = view[selectedFile];
            if (dataSave !== undefined){
                dataSave[selectedCond] = scaleRange;
            }
            else {
                dataSave = {};
                dataSave[selectedCond] = scaleRange;
            }
            view[selectedFile] = dataSave;

            var svg = d3.select(view.lookupReference('scaleEditor2').el.dom).select("#scaleEditor2");
            svg.selectAll("*").remove();

            svg = d3.select(view.lookupReference('scaleEditor2').el.dom).select("#scaleEditor2");

            metExploreD3.GraphNumberScaleEditor.createNumberScaleCaption(svg, width, height, margin, scaleRange);

            svg.on("click", function(){
    			var win = Ext.create("metExploreViz.view.form.fluxScaleEditor.FluxScaleEditor", {
                    scaleRange: scaleRange,
                    cond: 'second'
                });

    			win.show();
    		});
        }


    },

    /**
     * Reset scale graph
     * @param {String} cond select scale graph to reset
     * @param {Object} svgView view where the scale data is store
     */
    resetScale: function(cond, svgView){
        var me = this;
        var view = me.getView();

        var nbCol = view.lookupReference('selectColNumber').getValue();
        var selectedFile = view.lookupReference('selectFile').getValue();

        if (nbCol === "one"){
            var selectedCond = view.lookupReference('selectConditionFlux').getValue();
            var selectedCond2 = "null";

            var data = this.getFluxData(selectedFile, nbCol, selectedCond, selectedCond2, "flux");
            var fluxData = data[0];
            var targetLabel = data[1];

            var scaleRange = metExploreD3.GraphFlux.getScale(fluxData, targetLabel);
        }
        if (nbCol === "two"){
            var selectedCond = view.lookupReference('selectConditionFlux').getValue();
            var selectedCond2 = view.lookupReference('selectConditionFlux2').getValue();

            var data = this.getFluxData(selectedFile, nbCol, selectedCond, selectedCond2, "flux");
            var fluxData = data[0];
            var targetLabel = data[1];

            var fluxData1 = [];
            for (var i = 0; i < fluxData.length; i++){
                var tmp = [fluxData[i][0], fluxData[i][1]];
                fluxData1.push(tmp);
                var tmp = [fluxData[i][0], fluxData[i][2]];
                fluxData1.push(tmp);
            }

            var scaleRange = metExploreD3.GraphFlux.getScale(fluxData1, targetLabel);
        }

        svgView.scaleRange = scaleRange;
    }

});
