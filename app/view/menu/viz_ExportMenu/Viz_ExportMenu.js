/**
 * @author Fabien Jourdan
 * (a)description Menu export network viz
 */

Ext.define('metExploreViz.view.menu.viz_ExportMenu.Viz_ExportMenu', {
        extend: 'Ext.menu.Menu',
        alias: 'widget.vizExportMenu',
        id: 'exportMenu',
        requires: [
            'metExploreViz.view.menu.viz_ExportMenu.Viz_ExportMenuController',
            'metExploreViz.view.menu.viz_ExportMenu.Viz_ExportMenuModel',

        ],

        controller: "menu-vizExportMenu-vizExportMenu",
        viewModel: {
            type: "menu-vizExportMenu-vizExportMenu"
        },

        items:  [
             {
                 text: 'Export Viz as svg',
                 reference:'exportSVG',
                 iconCls:'exportSvg'
                },
            {
                text: 'Export Viz as png',
                scale: 'large',
                menu:{id:'vizExportPNG',xtype: 'vizExportPNG'},
                id:'vizExportPNGID',
                reference:'vizExportPNGID',
                padding:'0 0 0 0',
                iconCls:'exportPng'
            },
            {
                text: 'Export Viz as jpeg',
                scale: 'large',
                menu:{id:'vizExportJPG',xtype: 'vizExportJPG'},
                id:'vizExportJPGID',
                reference:'vizExportJPGID',
                padding:'0 0 0 0',
                iconCls:'exportJpg'
            },
            {
                text: 'Export Viz as HTML link',
                reference: 'exportHTML',
                iconCls: 'exportHtml',
                tooltip: 'Log in to enable this feature',
                disabled: true,
                hidden: false
            },
            {
                text: 'Save modification(s)',
                reference: 'saveHTML',
                iconCls: 'exportHtml',
                tooltip: 'unblock your network to enable modification save',
                disabled: true,
                hidden: true
            }
            // ,
            //     {
            //      text: 'Export Comparison of Condition',
            //      reference:'exportComparison',
            //      tooltip:'Export Comparison of Condition as a png file',
            //      iconCls:'exportComparePng'
            //     }
        ]
});
