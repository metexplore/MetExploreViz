#!/usr/bin/env node

const fs = require('fs');
const { execSync } = require('child_process');

// Récupérer l'argument de ligne de commande correspondant à la version
const versionArg = process.argv[2];

let newVersion;

newVersion = execSync(`npm version --no-git-tag-version ${versionArg}`).toString().trim();

// Lire le fichier package.json
const packageJson = JSON.parse(fs.readFileSync('package.json', 'utf8'));

// Mettre à jour la version dans le fichier YAML
const appFile = 'app.json';

// Lire le contenu du fichier YAML
let appFileContent = fs.readFileSync(appFile, 'utf8');

// Remplacer la version dans le contenu du fichier YAML
const updatedappFileContent = appFileContent.replace(/"version": "(.+)"/, `"version": "${packageJson.version}"`);

// Écrire le contenu mis à jour dans le fichier YAML
fs.writeFileSync(appFile, updatedappFileContent, 'utf8');

// execSync(`git add package.json ${appFile}`);
// execSync(`git commit -m "Update version of package and yaml files to ${newVersion}"`);
// execSync(`git tag ${newVersion}`);

console.log(`Successfully updated versions of package.json and ${appFile} to version ${newVersion}`);
