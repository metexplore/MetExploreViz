# MetExploreViz

MetExploreViz provides an interactive visualisation of metabolic networks in order to mine metabolomics (and other "omics") data.

## Install MetExploreViz as npm dependency

The npm module is available on the [forgemia web site](https://forgemia.inra.fr/metexplore/MetExploreViz/-/packages).
To be able to get it, create a .npmrc in your new project and add the url of the metexplore registry by typing:

```bash
echo @metexplore:registry=https://forgemia.inra.fr/api/v4/packages/npm/ >> .npmrc
```

Then you can install it:

```bash
npm i @metexplore/metexploreviz
```

You can now access to the js library in node_modules/@metexplore/metexploreviz/build/production/metExploreViz/metexploreviz.js.

## Developer documentation

### Change version number

Major version (X.y.z):

```bash
npm run update-version major
```

Minor version (x.Y.z):

```bash
npm run update-version minor
```

Patch version (x.y.Z):

```bash
npm run update-version patch
```

To a specific version:

```bash
npm run update-version 0.6.0
```

The command "npm run update-version" changes both package.json and app.json files.

### Local build

To use MetExploreViz in local environment, you have to build the application with [sencha command](https://www.sencha.com/products/extjs/cmd-download/) 
> sencha app build.

### Generate developer documentation  

To use generate developer documentation in local environment, you have to build the doc with [JS Duck](https://github.com/senchalabs/jsduck)  
> jsduck --config doc-conf.json --output docs
